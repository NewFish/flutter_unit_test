import 'package:flutter/material.dart';
import 'package:flutter_unit_test/main_page_logic.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Unit Test Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  MainPageLogic _logic;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.all(20),
        child: Form(
          child: TextFormField(
            autovalidate: true,
            validator: (input) => _logic.userNameValidator(input),
            maxLength: 20,
            decoration: InputDecoration(
              hintText: "输入你的用户名",
              prefixIcon: Icon(Icons.account_circle),
            ),
          ),
        ),
      ), //is trailing comma makes auto-formatting nicer for build methods.
    );
  }

  @override
  void initState() {
    super.initState();
    _logic = MainPageLogic();
  }
}
