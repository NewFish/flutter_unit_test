import 'package:test/test.dart';

void main(){

  String _userNameValidator(String input){
    assert(input != null);

    if(input.isEmpty){
      return "用户名不能为空";
    }

    //正则表达式
    Pattern pattern = '[^A-Za-z]';
    RegExp regex = new RegExp(pattern);
    if(regex.hasMatch(input)){
      return "用户名只能为纯英文";
    }

    if(input.length < 8){
      return "用户名不能少于8位";
    } else if(input.length > 20){
      return "用户名不能多于20位";
    }

    return null;
  }


  test("Unit Test", (){
    expect(_userNameValidator("abcdefghijk"), null);
  });

}